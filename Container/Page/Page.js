import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts} from "../../Component/store/actions/contactsAction";
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from "react-native";
import Contact from "../../Component/Contact/Contact";

const Page = () => {
    const contacts = useSelector(state => state.contacts.contacts);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchContacts())
    }, [dispatch]);

    return (
        <>
            <SafeAreaView style={styles.container}>
                <View>
                    <Text>My Contact</Text>
                </View>
                <ScrollView>
                    {contacts.map((contact) => {
                        return (
                            <Contact
                                key={contact.id}
                                name={contact.name}
                                photo={contact.photo}
                                email={contact.email}
                                phone={contact.phone}
                                id={contact.id}
                            />
                        );
                    })}
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 20,
        flex: 1,
        backgroundColor: "#fff",
        justifyContent: "flex-start",
    },
});

export default Page;