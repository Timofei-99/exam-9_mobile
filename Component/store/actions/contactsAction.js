import axiosAPI from "../../../axiosAPI";

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_FAILURE = 'FETCH_CONTACTS_FAILURE';

export const OPEN_MODAL = 'OPEN_MODAL';
export const openModal = () => ({type: OPEN_MODAL});

export const CLOSE_MODAL = 'CLOSE_MODAL';
export const closeModal = () => ({type: CLOSE_MODAL});


export const fetchContactsRequest = () => ({type: FETCH_CONTACTS_REQUEST});
export const fetchContactsSuccess = (value) => ({type: FETCH_CONTACTS_SUCCESS, payload: value});
export const fetchContactsFailure = () => ({type: FETCH_CONTACTS_FAILURE});


export const fetchContacts = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchContactsRequest());
            const response = await axiosAPI.get('contacts.json');
            const contact = Object.keys(response.data).map(item => ({
                ...response.data[item],
                item
            }));
            dispatch(fetchContactsSuccess(contact))
        } catch (error) {
            dispatch(fetchContactsFailure(error));
        }
    }
}