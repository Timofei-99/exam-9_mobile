import {
    CLOSE_MODAL,
    FETCH_CONTACTS_FAILURE,
    FETCH_CONTACTS_REQUEST,
    FETCH_CONTACTS_SUCCESS,
    OPEN_MODAL
} from "../actions/contactsAction";

const initialState = {
    contacts: [],
    modal: false,
}

const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONTACTS_REQUEST:
            return {...state}
        case FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.payload}
        case FETCH_CONTACTS_FAILURE:
            return {...state}
        case OPEN_MODAL:
            return {...state, modal: true}
        case CLOSE_MODAL:
            return {...state, modal: false}
        default:
            return state;
    }
};

export default contactsReducer;