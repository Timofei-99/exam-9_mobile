import React from "react";
import {
    SafeAreaView,
    ScrollView,
    Image,
    Text,
    Button,
    StyleSheet,
    Modal,
    View,
} from "react-native";

const MeModal = (props) => {
    return (
        <Modal>
            <SafeAreaView style={{ marginTop: 100 }}>
                <ScrollView>
                    <Text style={styles.heading}>{props.name}</Text>
                    <View>
                        <Image
                            source={{ uri: props.photo }}
                            style={{ width: 300, height: 300, marginLeft: 70 }}
                        />
                    </View>
                    <Text style={styles.text}>{props.email}</Text>
                    <Text style={styles.text}>{props.phone}</Text>
                </ScrollView>
                <View style={styles.btn}>
                    <Button onPress={() => props.closeModal()} title="Back to List" />
                </View>
            </SafeAreaView>
        </Modal>
    );
};

const styles = StyleSheet.create({
    heading: {
        fontSize: 45,
        margin: 15,
    },
    text: {
        fontSize: 30,
        margin: 10,
    },
    btn: {
        marginTop: 200,
    },
});

export default MeModal;
