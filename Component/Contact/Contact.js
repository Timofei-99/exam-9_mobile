import React, {useState} from "react";
import {Image, StyleSheet, Text, TouchableOpacity, View,} from "react-native";
import MeModal from "../Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {closeModal, openModal} from "../store/actions/contactsAction";

const Contact = (props) => {
    const dispatch = useDispatch();
    const modal = useSelector(state => state.contacts.modal)

    const close = () => {
        dispatch(closeModal());
    };
    const showModal = () => {
        dispatch(openModal());
    };
    return (
        <>
            {modal === true ? (
                <MeModal
                    key={props.id}
                    name={props.name}
                    photo={props.photo}
                    email={props.email}
                    phone={props.phone}
                    closeModal={close}
                />
            ) : null}
            <TouchableOpacity onPress={showModal}>
                <View style={styles.contact}>
                    <View>
                        <Image
                            source={{ uri: props.photo }}
                            style={{ width: 100, height: 100 }}
                        />
                    </View>
                    <View style={styles.block}>
                        <Text>{props.name}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </>
    );
};

const styles = StyleSheet.create({
    contact: {
        marginTop: 10,
        marginBottom: 20,
        flex: 1,
        backgroundColor: "#fff",
        flexDirection: "row",
        alignItems: "center",
        borderWidth: 2,
        borderColor: "#000",
        padding: 10,
    },
    block: {
        marginLeft: 70,
    },
});

export default Contact;
