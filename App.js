import React from "react";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import contactsReducer from "./Component/store/reducers/contactsReducer";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import Page from "./Container/Page/Page";

const App = () => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const rootReducer = combineReducers({
        contacts: contactsReducer,
    });

    const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));



    return (
        <Provider store={store}>
          <Page/>
        </Provider>

    );
};



export default App;
