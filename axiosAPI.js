import axios from "axios";

const axiosAPI = axios.create({
    baseURL: 'https://exam-9-905c2-default-rtdb.firebaseio.com/',
});

export default axiosAPI;